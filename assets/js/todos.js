$("ul").on("click", "li", function(){
    $(this).toggleClass("completed");
});

$("ul").on("click", "span", function(ev){
    $(this).parent().fadeOut(750, function(){
        $(this).remove();
    });
    ev.stopPropagation();
});

$("#addToDo").on("keypress", function(ev){
    if (ev.which === 13){
        var contents = $(this).val();
        $("ul").append("<li> <span><i class='fas fa-trash-alt'></i></span> " + contents + "</li>");
        $(this).val("");
    }
});

$(".fa-clipboard-list").on("click", function(){
    $("#addToDo").fadeToggle(500);
});